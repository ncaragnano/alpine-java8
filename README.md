# README #

### What is this repository for? ###

It contains the Dockerfile that can be used to build a docker image based on alpine:edge with
Java 8 OpenJDK.

### How to build the docker image

Simply execute the following command from the root of the project

    $ docker build --tag=alpine-java8:base --rm=true .
    
**NOTE**
In order to build successfully the docker image, you need the Docker command line. Please
refer to https://docs.docker.com/engine/reference/commandline/cli/  